Software Design Document
This project is to test out bit bucket and get our repository started. We will have to clone it onto our local machine then 
learn how to add files, commit it with comments, and then push it into the server. If we are on a different computer we will 
pull first to make sure we are up to date with the most current version of the repository. Also make sure to use tags to
keep track of your commits. For instance, after finishing stage one of a project we will first commit then add a tag to
inform of the end of stage one.

git add filename.md
git commit -m "message"
git push origin master
git pull origin master

git tag final
git push --tags